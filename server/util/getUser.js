
var jwt      = require('jsonwebtoken'),
    params   = require('../../config/paramS');

function getUser(token, callback){
    jwt.verify(token, params.fourtytwo, function(err, decoded) {
        if (err) {
            console.log('routeUser : util : getUser : verify '.bgRed);
            console.log(err);
            callback(err, false);
        } else {
            console.log('verify'.bgCyan);
            callback(decoded.username, true);
        }
    });
}


module.exports = {
    getUser : getUser
};